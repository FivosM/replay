--[[ 
Index table structure:
    self["index"][tick][tick information (0 is reserved for time difference)]

]]--

local replays = {}

local serialize = require("dependencies/binser")

local function closestToTime(n1, n2, n3) -- Return the number which is closest to n1
    if n2 == n3 or n3 == nil then return n2 end
    local average = (n2 + n3) / 2
    if n1 < average then 
        return math.min(n2, n3)
    end
    return math.max(n2, n3)
end

local function timeDifference(self, t1, t2)
    return math.abs(t1-t2)
end

local function newTick(self)
    self["index"][#self["index"]+1] = {}
    self["index"][#self["index"]][0] = love.timer.getTime() - self["index"][#self["index"]-1][0]
end

local function getValueTick(self, variable, tick)
    if tick == nil then tick = #self["index"] end
    return self["index"][tick][variable]
end

local function getValueTime(self, variable, time)
    local totaltime = 0
    for i = 1, #self["index"] do
        totaltime = totaltime + self["index"][i][0]
            if totaltime > time then
                return self["index"][i][variable]
            end
    end
end

local function updateValue(self, variable, value, tick )
    if tick == nil then tick = #self["index"] end
    self["index"][tick][variable] = value
end

local function encodeRecording(self)
    return serialize.serialize(self["index"])
end

local function decodeRecording(encodedRecording)
    return serialize.deserialize(encodedRecording)
end

local function saveRecording(self, file)
    serialize.appendFile(file, self["index"])
end

local function loadRecording(self, file)
    self["index"] = serialize.readFile(file)
end

function replays.new()
    local index = {[0] = {[0] = 0}}
    return {
        newTick = newTick,
        updateValue = updateValue,
        getValueTick = getValueTick,
        getValueTime = getValueTime,
        timeDifference = timeDifference,
        encodeRecording = encodeRecording,
        decodeRecording = decodeRecording,
        saveRecording = saveRecording,
        loadRecording = loadRecording,
        index = index
    }
end

return replays
