--TESTS

replays = require("replay")

rec = replays.new()
rec:newTick()
rec:updateValue("hi", 12 )
local time = love.timer.getTime()
rec:newTick()
rec:updateValue("hi", 13 )
print(rec:getValueTick("hi"))
print(rec:getValueTime("hi", time))
rec:saveRecording("file.txt")

rec:loadRecording("file.txt")
print(rec:getValueTick("hi"))
print(rec:getValueTime("hi", time))

function love.load()

end

function love.update()

end

function love.draw()

end